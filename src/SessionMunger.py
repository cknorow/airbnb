import os
import datetime
import copy
import numpy as np
import pandas as pd



from BaseMunger import Munger as BaseMunger
import engineer

class SessionMunger(BaseMunger):
	def load_data(self, M):
		self.M = M

		#Loading data
		df = pd.read_csv(os.path.join(self.basedir, 'sessions.csv')).fillna(-1)
		df['action'] = df['action'].apply(str)
		df['action_type'] = df['action_type'].apply(str)
		df['action_detail'] = df['action_detail'].apply(str)
		
		self.df_session = copy.deepcopy(df)
		
		self.df = engineer.engineer_column2strings(df.groupby('user_id'), =['action', 'action_detail','action_type', 'device_type'])
		self.df =  pd.merge(self.M.df, self.df, how='right', on='user_id')

	def engineer_features(self, df=None, params = {'min_df':.02, 
												   'ngram_range': (1,3),
												   'max_df':.7 },
											  N = 3):
		df = self.df_checker(df)

		#apply to the intial dataframe
		df = pd.merge(df, engineer.engineer_statistics(self.df_session.groupby('user_id'), 'secs_elapsed'), how='left', on='user_id')
		
		df = engineer.engineer_sliding_window(df, columns=['action'], N=N)

		self.df = df

		self.sparse_words = engineer.engineer_word_count( df, columns = ['action',
															  'action_type', 
															  'action_detail']+
															  map(lambda x: 'action_{}'.format(x), range(N)),
													     **params)
		
		
	
	def get_training_labels(self):
		return pd.merge(self.df, self.M.get_training_labels(), how='inner', on='user_id' )[['user_id','y_train']]


def clean_data(UserMunger):
	S = SessionMunger()
	S.load_data(UserMunger)
	S.engineer_features()
