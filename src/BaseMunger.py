import os
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder


class Munger():
	''' This is the  base data munging class which has useful features to operate on a dataframe
		stored in itself '''
	def __init__(self, basedir='~/Kaggle/airbnb/data'):
		np.random.seed(0)
		self.basedir = os.path.expanduser(basedir)
		self.le = LabelEncoder()

	def get_training_labels(self):
		return pd.merge(self.df[self.df.set=='Train'], self.labels, on='user_id', how='right')[['user_id','y_train']]

	def data_split(self, df=None):
		''' split data into the original train and test data sets 
		\returns X -  multidimensional array of training features
		\returns X_test - multidimensional array of test features
		'''
		if df is None:
			df = self.df

		train_set = df['set']=='Train'
		test_set = df['set']=='Test'
		df = df.drop(['set'], axis=1)

		X = df[train_set]
		X_test = df[test_set]

		return X, X_test,

	def df_checker(self, df):
		""" the purpose of the checker is to return the self.df object
		if the user has passed df=None, the reason is this way the user can
		also pass other df and make use of the same structures"""
		if df is None:
			df = self.df
		return df

	def drop_features(self, df=None, columns=[]):
		''' drops features from data frame
			\param df - data frame object or None, None is self (default: None)
			\param columns - array of columns to drop from df
		'''
		df = self.df_checker(df)

		for column in columns:
			df = df.drop([column], axis=1)

		self.df = df


	def one_hot_encode(self, df=None, ohe_feats=[]): 
		''' one hot encodes features in df
		\param df - data frame object or None, None is self (default: None)
		\param ohe_feats - list of columns apply to one hot encoding too
		'''
		df = self.df_checker(df)

		#One-hot-encoding features
		for f in ohe_feats:
		    df_dummy = pd.get_dummies(df[f], prefix=f)
		    df = df.drop([f], axis=1)
		    df = pd.concat((df, df_dummy), axis=1)

		self.df = df

	def label_transformer(self, labels=None, column='y_train'):
		if labels == None:
			labels = self.labels
			return self.le.fit_transform(labels[column]) 
		return self.le.transform(labels[column]) 


	def label_inverse_transformer(self, labels):
		return self.le.inverse_transform(labels) 

	def engineer_features(self, df=None):
		''' feauture engineering to perform on data frame'''
		pass
