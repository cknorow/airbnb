import random
import numpy as np

def get_random_frequency_sampling(N = 100, b = .6):
    samples = []
    for i in range(N):
        if random.randint(0,10) in range(3,10):
            samples.append(1)
        else:
            samples.append(0)
    return np.array(samples)

def eigen_label(x, eigen_labels=[], eigen_values=None):
	''' given a value x, we perform a mapping transformation to a lower dimensional space
	
	\param x - value to map
	\param eigen_labels - array of arrays of classes, [[class1, class3],[class2]]
	\param eigen_values - names to map to - default (None)

	\example

	eigen_label(1, [[7],[0,1,2,3,4]])
	>>> 1

	eigen_label('whiskey', eigen_labels=[['whiskey','margarita'],['pizza','burger',10], eigen_values=['drink','food'] )
	>>>> drink
	'''
	if eigen_values is None:
		eigen_values = range(len(eigen_labels))

	if len(eigen_values) != len(eigen_labels):
		print 'eigen values must have same length as eigen labels'
		raise Exception

	for i in range(len(eigen_labels)):
		if x in eigen_labels[i]:
			return eigen_values[i]



#### THese need to be fixed for general uses
def print_logistic_imporatance(clf, column):
    importances = clf.coef_[0]
    indices = np.argsort(importances)[::-1]

    # Print the feature ranking
    print("Feature ranking:")

    for f in range(X_train.shape[1]):
        print("%d. feature %s (%f)" % (f + 1, S.sparse_words[column][f], importances[indices[f]]))
        if f == 100:
            break

def print_forest_importance(clf, column):
	importances = clf.feature_importances_
	std = np.std([tree.feature_importances_ for tree in clf.estimators_],
	             axis=0)
	indices = np.argsort(importances)[::-1]

	# Print the feature ranking
	print("Feature ranking:")

	for f in range(X_train.shape[1]):
	    print("%d. feature %s (%f)" % (f + 1, S.col_names[f], importances[indices[f]]))
	    if f == 100:
	        break