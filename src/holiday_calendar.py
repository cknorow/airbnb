import pandas as pd
import holidays

def get_holiday_dict(years = [2009, 2010, 2011, 2012, 2013, 2014]):
	US_holiday = {}
	for date, name in sorted(holidays.US(years = years).items()):
	    if 'Observed' not in name:
	        US_holiday.setdefault(name,[])
	        US_holiday[name].append(date)

	for key in US_holiday.keys():
	    US_holiday[key] = pd.DataFrame(US_holiday[key])

	return US_holiday

def get_holiday_dates(years = [2009, 2010, 2011, 2012, 2013, 2014]):
	US_holiday_dates = []
	for date, name in sorted(holidays.US(years = years).items()):
	    US_holiday_dates.append(date)

	return pd.DataFrame(US_holiday_dates)