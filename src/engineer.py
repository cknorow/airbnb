import datetime
import pandas as pd
import numpy as np

from holiday_calendar import get_holiday_dates
from sklearn.feature_extraction.text import CountVectorizer

def engineer_dates( df, column):
	''' given a date column return new columns which have parsed out
		the date, weekeday, eyar, month day and hour 

		\param df 
		\param column - the column to apply the date parser on
	'''

	def convert_time(time_string):
		return datetime.datetime.strptime(str(time_string), '%Y%m%d%H%M%S')

	def weekend_or_weekday(x):
		if x.weekday() in range(1,6):
			return 1
		else:
			return 0

	#timestamp_first_active
	tfa = df[column].apply(convert_time)
	df['{}_date'.format(column)]     = tfa.apply(lambda x: x.date())
	df['{}_weekday'.format(column)]  = tfa.apply( weekend_or_weekday )
	df['{}_year'.format(column)]     = tfa.apply( lambda x: x.year)
	df['{}_month'.format(column)]    = tfa.apply( lambda x: x.month)
	df['{}_day'.format(column)]      = tfa.apply( lambda x: x.day)
	df['{}_hour'.format(column)]     = tfa.apply( lambda x: x.hour)

	return df

def engineer_bins(df, column, base=5,  min_bin=15, max_bin=70, nan_value=-1):
	''' separate ages into buckets, ages below a max, min and undefined are set to -1 
	\param df - pandas DataFrame
	\param column - column with age values
	\param base - bucket size (default 5)
	\param min_age - default (17) 
	\param max_age - default (70)
	'''

	def myround(x,  base=10, min_bin=0, max_bin=100, nan_value=-1):
	    if x < min_bin or x > max_bin:
			return nan_value
	    return int(base * round(float(x)/base))

	df[column] =  df[column].fillna(-1).apply(myround, args=(base, min_bin, max_bin, nan_value))

	return df
	
def engineer_holidays(df, column):
	''' adds a column containing the number of days until the next US holiday 
	\param df - Pandas DataFrame
	\param column - column to operate on (must be a datetime object)'''
	def compute_date_diff(x, dates):
	    r = dates - x
	    return r[r[0]>=0].min().iloc[0].days

	US_holidays = get_holiday_dates()    
	df['delta_holiday'] = df[column].apply(compute_date_diff, args=(US_holidays,))
	
	return df

def engineer_statistics(A, column, stats = [len, np.sum, np.mean, np.std, np.median]):
	'''create statistics on a specific column of a grouped data frame
	   \param A - df.groupby('something')
	   \param column - the column to get statistics on 
	   \param stats - the operations to apply to that column default [len, np.sum, np.mean, np.std, np.median]

	   \returns - a df with the default index that was grouped on and columns with names len_<column> ,...engineer_count
	'''
	def column_mapper(column, columns, index='user_id'):
		column_map = {}
		for key in columns:
		    if key != index:
		        column_map[key] = key+'_{}'.format(column)
		return column_map
    
	df = A[column].agg(stats).reset_index()
	df = df.rename(columns=column_mapper(column, df.columns))

	return df

def engineer_column2strings(A, columns):
    r =  A.aggregate(lambda x: ' '.join(x))
    return pd.DataFrame(zip(r.index, *map(lambda x: r[x], columns)), columns = ['user_id'] + columns) 

def engineer_word_count(df, columns=['action', 'action_detail','action_type'], **params):
 	word_list = {}	
	for column in columns:
		A = df[column].fillna('').tolist()
		V = CountVectorizer(**params)
		V.fit(A)
		word_list[column] =  [V.transform(A), V.get_feature_names()]
	return word_list

def engineer_sliding_window(df, columns=['action'], N=3):
    def single_window(x, index, N = 3):
        P = x.split()
        delta = len(P)/N
        return ' '.join(P[index*delta:(index+1)*delta])
    
    for column in columns:
	    for i in range(N):
	        df['{}_{}'.format(column, i)] = df[column].apply(single_window, args=(i,N))
        
    return df