import os
import datetime
import copy
import numpy as np
import pandas as pd

import engineer
from BaseMunger import Munger as BaseMunger


class UserMunger(BaseMunger):

	def load_data(self, train_path='train_users_2.csv', test_path = 'test_users.csv' ):

		#Loading data
		df_train = pd.read_csv(os.path.join(self.basedir, train_path))
		df_test = pd.read_csv(os.path.join(self.basedir, test_path))
		
		df_train = df_train.rename(columns={'id':'user_id'})
		df_test = df_test.rename(columns={'id':'user_id'})
		
		self.labels = copy.deepcopy(df_train[['user_id', 'country_destination']])
		self.labels = self.labels.rename(columns={'country_destination':'y_train'})

		df_train = df_train.drop(['country_destination'], axis=1)

		df_train['set']  = 'Train'
		df_test['set']   = 'Test'

		#Creating a DataFrame with train+test data
		df = pd.concat((df_train, df_test), axis=0, ignore_index=True)

		#Filling nan
		self.df = df.fillna(-1)



	def engineer_features(self, df=None):
		df = self.df_checker(df)

		df = engineer.engineer_dates(df, 'timestamp_first_active')
		df = engineer.engineer_bins(df, 'age' )
		df = engineer.engineer_holidays(df, 'timestamp_first_active_date')

		self.df = df

	
def clean_data(one_hot=False):
	M = UserMunger()
	M.load_data()
	M.engineer_features()
	M.drop_features(columns = ['timestamp_first_active', 
				   				'date_account_created', 
				   				'tfa_day', 
				   				'tfa_month',
				   				#'id', 
				   				'date_first_booking',
				   				'tfa_date',
				])
	M.drop_features(columns = ['first_browser',
				 'first_device_type',
			         #'signup_flow', 
				 'affiliate_provider', 
				 'first_affiliate_tracked', 

				])

	if one_hot:
		M.one_hot_encode(ohe_feats = [	'gender', 
						'signup_method',
						'language', 
						'affiliate_channel', 
						'signup_app',
						'age' 
										]) 
	return M
